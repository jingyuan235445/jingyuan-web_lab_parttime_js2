"use strict";

// FUNCTIONS
// ------------------------------------------

// TODO Complete this function, which should generate and return a random number between a given lower and upper bound (inclusive)
function getRndInteger(min, max) {
    return Math.floor(Math.random()*(max-min+1)) + min;
}

console.log(getRndInteger(25, 50));
// Math.floor(Math.random()) -> 0 - 0
// Math.floor(Math.random() * (20-2)) -> 0 - 17
// Math.floor((Math.random() * (20-2+1))) -> 0 - 18
// Math.floor(Math.random() * (20-2+1)) + 2 -> 2 - 20

// TODO Complete this function, which should round the given number to 2dp and return the result.
function roundTo2dp(number) {
    return parseInt(number*100)/100;
}

console.log(roundTo2dp(4.457865));
// number*100 = 445.7865
// parseInt(number*100) = 445

// TODO Write a function which calculates and returns the volume of a cone.
/*function cone(input) {
    let [r,h]= input.map(Number);
    let s = Math.sqrt(r*r +h*h);
    let volume =Math.PI *r*r*h/3;
    console.log("volume = " + volume);
    // let area = Math.PI*r*(r+s);
    // console.log("area = " + area);
}

cone(["3.3", "7.8"]);*/

function cone(r, h) {
    var volume =Math.PI *r*r*h/3;
    var x=parseInt(volume*100)/100;
    return x;
}
console.log("volume = "+cone("25","40"));
// TODO Write a function which calculates and returns the volume of a cylinder.
function cylinder(r,h){
    var volume = Math.PI *r*r*h;
    var y =parseInt(volume*100)/100;
    return y;
}
console.log("volume = " + cylinder(30,40));
/*function Cylinder(cyl_height, cyl_diameter) {
    this.cyl_height = cyl_height;
    this.cyl_diameter = cyl_diameter;
  }
  
  Cylinder.prototype.Volume = function () {
    var radius = this.cyl_diameter / 2;
    return this.cyl_height * Math.PI * radius * radius;
  };
  
  var cyl = new Cylinder(7, 4);
  // Volume of the cylinder with four decimal places.
  console.log('volume ='+ cyl.Volume().toFixed(4));*/
  

// TODO Write a function which prints the name and volume of a shape, to 2dp.
function print(name,x) {
    console.log(
        "The volume of the " + name + " is:"+ x +" cm^3"
    );
} 

print("cone", cone(25, 40));

function print1(name,y){
    console.log(
        "The volume of the " + name +" is:"+ y +" cm^3"
    );
}
print1("cylinder",cylinder(30,40));
// ------------------------------------------

// TODO Complete the program as detailed in the handout. You must use all the functions you've written appropriately.
function print2(name,y){
    console.log(
        "The shape with the largest volume is "+ name + ", with a volume of " + y +" cm^3"
    );
}
print2("cylinder",cylinder(30,40));