"use strict";

// Provided variable
var theString = "Hello World!";

// Variable you'll be modifying
var reversed = "";

// TODO Your answer here.
for (var i=theString.length-1; i>-1; i--){
    reversed = reversed + theString[i];
}
// when i=0, theString[0] = 'H'
// when i=1, theString[1] = 'e' ....


// Printing the answer.
console.log("\"" + theString + "\", reversed, is: \"" + reversed + "\".");

var numbers = [-9, 2, 7, 5, 124, -5, 1, 144];

numbers.reverse();

